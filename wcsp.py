"""
Class WCSP
"""

import operator
import random
import sys
from math import ceil
from subprocess import check_output

import numpy as np

from constraints import Constraint


class WCSP:
    """
    class WCSP
    """
    def __init__(self):
        """
        Intialize a WCSP instance
        """
        self.constraints = {}
        self.mdom = 0  # the maximum domain
        self.var_size = 0  # the number of variables
        self.bound = 0  # the upper bound
        self.doms = []  # the domains of variables

    def read_wcsp(self, filename=''):
        """
        Update the instance from a given input file
        param filename: path to the input file
        """
        with open(filename, 'r') as f:
            # first line
            line = f.readline().strip().split()
            self.var_size, self.mdom, _, self.bound = list(map(int, line[1:]))
            # second line specifies domains of variables
            self.doms = list(map(int, f.readline().strip().split()))
            while True:
                line = f.readline()
                if not line:
                    break
                line = line.strip().split()
                num_vars = int(line[0])
                con_vars = list(map(int, line[1: num_vars + 1]))
                sorted_idx = np.argsort(con_vars)
                sorted_vars = [con_vars[idx] for idx in sorted_idx]
                def_cost = int(float(line[-2]))
                assign_num = int(line[-1])
                self.constraints[tuple(sorted_vars)] = Constraint(sorted_vars,
                                                                  def_cost)

                for _ in range(assign_num):
                    line = f.readline().strip().split()
                    assign = tuple([int(line[idx]) for idx in sorted_idx])
                    cost = int(float(line[-1]))
                    self.constraints[tuple(sorted_vars)].set_weight(assign, cost)

    def write_wcsp(self, filename='', K2=10, opt=None):
        """
        write to a file in order to call WCSPLift solver
        param filename: path to the output file
        param K2: number of additional constraints
        param opt: best solutions from previous runs
        """
        K1 = 5
        K2 = min(K2, int(ceil(self.var_size / K1)))
        cons_num = len(self.constraints)

        cons_str = []
        if opt:
            tvars = list(range(self.var_size))
            # randomize order of variables
            random.shuffle(tvars)

            for i in range(K2):
                temp_assigns = set()
                temp_sorted = sorted(tvars[i * K1 : (i + 1) * K1])

                for assign, _w in opt:
                    tup = tuple([assign[j] for j in temp_sorted])

                    temp_assigns.add(tup)

                    # temporary constraint
                if tuple(temp_sorted) in self.constraints:
                    temp_con = self.constraints[tuple(temp_sorted)]
                else:
                    temp_con = Constraint(temp_sorted)
                    cons_num += 1

                for assign in temp_assigns:
                    # the cost of this constraint is the upper bound
                    temp_con.set_weight(assign, self.bound)

                if tuple(temp_sorted) not in self.constraints:
                    cons_str.append(temp_con.output())

        cons_str += [_c.output() for _c in self.constraints.values()]

        with open(filename, 'w+') as ofile:
            ofile.write('wcsp {0} {1} {2} {3}\n'.format(self.var_size, self.mdom,
                                                        cons_num, self.bound))
            ofile.write(' '.join(list(map(str, self.doms))) + '\n')

            ofile.write('\n'.join(cons_str))

    @staticmethod
    def run_toulbar(filename, solver_path, param):
        """
        Call toulbar2
        param filename: wcsp file
        param solver_path: path to the binary file
        param param: parameters of the solver
        """

        sol_path = filename.split(".")[0]+".sol"
        ret = check_output([solver_path, filename, "-w=" + sol_path] +
                           [p for p in param]).decode("utf-8")

        with open(sol_path, "r") as sol_file:
            line = sol_file.readline()

            assignment = list(map(int, line.strip().split()))

            ret = ret.split("\n")

            cost = 0
            if ret[-1].startswith("Optimum"):
                cost = int(ret[-1].split(" ")[1])

            return [assignment, cost]


    @staticmethod
    def run_solver(filename, solver_path, vs, param):
        """
        Call WCSPLift
        param filename: wcsp file to run
        param solver_path: path to the wcsp solver
        param param: parameters of the solver
        """
        ret = check_output([solver_path] + [p for p in param] +
                           [filename]).decode('utf-8')
        ret = ret.split('\n')
        assignment = [0 for _ in range(vs)]
        counter = 0
        for i, line in enumerate(ret):
            if line.startswith('Best assignments:'):
                counter = i
                break

        if counter == len(ret) - 1:
            # in case the solution is not available
            sys.exit(-1)

        for i in range(counter + 2, len(ret) - 2):
            var, val = ret[i].strip().split()
            assignment[int(var)] = int(val)

        return [assignment, int(float(ret[-2].strip().split(':')[1]))]

    def get_topk(self, K, K3, filename, solver_path, param, K2=10,
                 istoulbar=False):
        """
        Get top K wcsp results
        param K: the number of results
        param K3: number of times to randomly generate a subset of variables
        param solver: path to the solver
        """
        opt = []
        for i in range(K):
            if i == 0:
                self.write_wcsp(filename)
                if istoulbar:
                    opt.append(WCSP.run_toulbar(filename, solver_path,
                                                param))
                else:
                    opt.append(WCSP.run_solver(filename, solver_path,
                                               self.var_size, param))
            else:
                assigns = []
                for _i in range(K3):
                    self.write_wcsp(filename, opt=opt, K2=K2)
                    if istoulbar:
                        assigns.append(WCSP.run_toulbar(filename, solver_path,
                                                        param))
                    else:
                        assigns.append(WCSP.run_solver(filename, solver_path,
                                                       self.var_size, param))
                assigns.sort(key=operator.itemgetter(1))
                # add the optimal assignment to solutions
                opt.append(assigns[0])
        return opt

    @staticmethod
    def output_sols(opt):
        """
        Write solutions
        """
        # print all solutions
        for i, assign in enumerate(opt):
            print('Solution {0}'.format(i+1))
            print('Weight {0}'.format(assign[1]))
            for j, val in enumerate(assign[0]):
                print('{0} {1}'.format(j, val))
            print('------------')
