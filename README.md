Top K Solutions Generator
------------------------

1. Compile `wcsp-solver` (The README file contains instructions).
2. Run `main.py` to generate top K solutions of a given WCSP instance. The path of input file (with DIMACS format, example can also be found in the `wcsp-solver` folder) and the path to the binary of the solver are necessary.
3. Run `python main.py -h` to understand the parameters.
